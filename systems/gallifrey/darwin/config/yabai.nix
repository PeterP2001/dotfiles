{pkgs, ...}:{
  services.yabai = {
    enable = true;
    package = pkgs.yabai;
    enableScriptingAddition = true;
    config = {
      focus_follows_mouse          = "off";
      mouse_follows_focus          = "on";
      window_placement             = "second_child";
      window_opacity               = "off";
      window_opacity_duration      = "0.0";
      window_border                = "on";
      window_border_placement      = "second_child";
      window_border_width          = 2;
      window_border_radius         = 3;
      active_window_border_topmost = "off";
      window_topmost               = "on";
      active_window_opacity        = "1.0";
      normal_window_opacity        = "1.0";
      split_ratio                  = "0.50";
      auto_balance                 = "on";
      mouse_modifier               = "fn";
      mouse_action1                = "move";
      mouse_action2                = "resize";
      layout                       = "bsp";
      top_padding                  = 12;
      bottom_padding               = 12;
      left_padding                 = 12;
      right_padding                = 12;
      window_gap                   = 12;

};

    extraConfig = ''
        # rules
        yabai -m rule --add app='System Preferences' manage=off
        yabai -m config external_bar all:$(sketchybar --query bar | jq .height):0

        /opt/homebrew/bin/borders active_color=0xff8aadf4 style=round width=8.0 inactive_color=0xff6e738d

	'';
    };
}
