{ pkgs, lib, ... }:

{
  # Enable zsh program
  programs.zsh.enable = true;


 system.activationScripts.applications.text = lib.mkForce ''
  echo "setting wallpaper"
  osascript -e 'tell application "Finder" to set desktop picture to POSIX file "/Users/peter/Git/dotfiles/systems/gallifrey/wallpapers/Wallpaper.png"' 
 '';



  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;
 imports = [
    ./config/skhd.nix
    ./config/yabai.nix
 ];

  # Define system packages
  environment.systemPackages = with pkgs; [
    fastfetch
    coreutils
    eza
    raycast
    ranger
    zulu17
    jq
    vscode
    skhd # NEEDED FOR YABAI
    yabai
    tmux
    youtube-dl
    htop
    git
    nerdfonts
  ];

  # Homebrew settings
  homebrew = {
    enable = true;

    onActivation.autoUpdate = true;
    onActivation.upgrade = true;

    # Homebrew taps
    taps = [
      "homebrew/cask-fonts"
      "FelixKratz/formulae"
];

    # Homebrew casks
    brews = [
     "borders"
     "sketchybar"
    ];
    casks = [
      "kitty"
      "font-0xproto-nerd-font"
      "arc"
      "microsoft-office"
      "modrinth"
      "curseforge"
      "discord"
      "steam"
      "gimp"
      "minecraft"
      "obsidian"
      "whatsapp"
      "telegram"
      "font-3270-nerd-font"
      "font-agave-nerd-font"
      "font-anonymice-nerd-font"
      "font-arimo-nerd-font"
      "font-aurulent-sans-mono-nerd-font"
      "font-bigblue-terminal-nerd-font"
      "font-bitstream-vera-sans-mono-nerd-font"
      "font-blex-mono-nerd-font"
      "font-caskaydia-cove-nerd-font"
      "font-caskaydia-mono-nerd-font"
      "font-code-new-roman-nerd-font"
      "font-comic-shanns-mono-nerd-font"
      "font-commit-mono-nerd-font"
      "font-cousine-nerd-font"
      "font-d2coding-nerd-font"
      "font-daddy-time-mono-nerd-font"
      "font-dejavu-sans-mono-nerd-font"
      "font-droid-sans-mono-nerd-font"
      "font-envy-code-r-nerd-font"
      "font-fantasque-sans-mono-nerd-font"
      "font-fira-code-nerd-font"
      "font-fira-mono-nerd-font"
      "font-geist-mono-nerd-font"
      "font-go-mono-nerd-font"
      "font-gohufont-nerd-font"
      "font-hack-nerd-font"
      "font-hasklug-nerd-font"
      "font-heavy-data-nerd-font"
      "font-hurmit-nerd-font"
      "font-im-writing-nerd-font"
      "font-inconsolata-go-nerd-font"
      "font-inconsolata-lgc-nerd-font"
      "font-inconsolata-nerd-font"
      "font-intone-mono-nerd-font"
      "font-iosevka-nerd-font"
      "font-iosevka-term-nerd-font"
      "font-iosevka-term-slab-nerd-font"
      "font-jetbrains-mono-nerd-font"
      "font-lekton-nerd-font"
      "font-liberation-nerd-font"
      "font-lilex-nerd-font"
      "font-martian-mono-nerd-font"
      "font-meslo-lg-nerd-font"
      "font-monaspace-nerd-font"
      "font-monocraft-nerd-font"
      "font-monofur-nerd-font"
      "font-monoid-nerd-font"
      "font-mononoki-nerd-font"
      "font-mplus-nerd-font"
      "font-noto-nerd-font"
      "font-open-dyslexic-nerd-font"
      "font-overpass-nerd-font"
      "font-profont-nerd-font"
      "font-proggy-clean-tt-nerd-font"
      "font-recursive-mono-nerd-font"
      "font-roboto-mono-nerd-font"
      "font-sauce-code-pro-nerd-font"
      "font-shure-tech-mono-nerd-font"
      "font-space-mono-nerd-font"
      "font-symbols-only-nerd-font"
      "font-terminess-ttf-nerd-font"
      "font-tinos-nerd-font"
      "font-ubuntu-mono-nerd-font"
      "lunar-client"
      "font-ubuntu-nerd-font"
      "font-ubuntu-sans-nerd-font"
      "font-victor-mono-nerd-font"
      "font-zed-mono-nerd-font"
    ];

    masApps = {
      "DaVinci Resolve" = 571213070;
      "Final Cut Pro" = 424389933;
      "Compressor" = 424390742;
    };
  };
  system = {
    keyboard.enableKeyMapping = true;
    keyboard.remapCapsLockToEscape = true;

    defaults = {  
      NSGlobalDomain = {
        # Dark mode
        AppleInterfaceStyle = "Dark"; # Light Dark 
        
        # Show all file extensions
        AppleShowAllExtensions = true;

        _HIHideMenuBar = false;
      };
      dock = {
        # Automatically hide and show the Dock
        autohide = true;
        
        # Style options
        orientation = "bottom"; # left right bottom
        show-recents = false;
        minimize-to-application = true;
        tilesize = 32;
        persistent-apps = [
          "/Applications/Arc.app"
          "/Applications/kitty.app"
          "/Applications/Discord.app"
          "/Applications/Microsoft\ Outlook.app"
          "/Applications/Microsoft\ Word.app"
          "/Applications/Microsoft\ PowerPoint.app"
          "/Applications/DaVinci\ Resolve.app"
          "/Applications/Final\ Cut\ Pro.app"
          "/Applications/Minecraft.app"
          "/Applications/CurseForge.app"
          "/Applications/Modrinth\ App.app"
        ];
      };
    };
  };

}
