{pkgs, lib, ...}:{


  home.stateVersion = "23.11";
  home = {
    username = "peter";
    homeDirectory = lib.mkForce "/Users/peter";
    packages = with pkgs; [

 ];
};
  imports = [
  ./config/nvim.nix
  ./config/kitty.nix
  ./config/zsh.nix
];


}
