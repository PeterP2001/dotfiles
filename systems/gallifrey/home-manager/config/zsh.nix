{pkgs, ...}:{
  programs = {
    zsh = {
      enable = true;
      enableCompletion = true;
      syntaxHighlighting = {
        enable = true;
      };
      shellAliases = {
        # CHANGE LS TO EZA 
        ls = "eza -al --color=always --group-directories-first";
        la = "eza -a --color=always --group-directories-first";
        ll = "eza -l --color=always --group-directories-first";

        nixswitch = "darwin-rebuild switch --flake ~/dotfiles/.#";
        
        nix-config="vim $HOME/dotfiles";

        df = "df -h";
      };
      oh-my-zsh = {
       enable = true;
       plugins = [ "git" ];
       theme = "gallifrey";
  };
      initExtra = ''
       clear
       fastfetch
       PATH="/opt/homebrew/bin:$PATH"
      '';
    };
    starship = {
	enable = true;
    };
  };

}
