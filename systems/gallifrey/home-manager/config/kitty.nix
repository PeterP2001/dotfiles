{ pkgs, config, ... }:{
    home.file.".config/kitty/kitty.conf".text = ''


font_family    JetBrainsMono Nerd Font
window_padding_width 6
font_size 13.0
hide_window_decorations titlebar-only
window_margin_width 4
cursor_blink_interval 0
macos_quit_when_last_window_closed no
macos_colorspace default
macos_show_window_title_in window
repaint_delay 8
input_delay 1
resize_draw_strategy blank
remember_window_size no
resize_debounce_time 0.001
confirm_os_window_close -2

tab_bar_edge top
tab_bar_style powerline
tab_powerline_style slanted
tab_activity_symbol 
tab_title_max_length 30
tab_title_template "{fmt.fg.red}{bell_symbol}{fmt.fg.tab} {index}: ({tab.active_oldest_exe}) {title} {activity_symbol}"

# The basic colors
foreground              #CAD3F5
background              #24273A
selection_foreground    #24273A
selection_background    #F4DBD6

# Cursor colors
cursor                  #F4DBD6
cursor_text_color       #24273A

# URL underline color when hovering with mouse
url_color               #F4DBD6

# Kitty window border colors
active_border_color     #B7BDF8
inactive_border_color   #6E738D
bell_border_color       #EED49F

# OS Window titlebar colors
wayland_titlebar_color  #24273A
macos_titlebar_color    #24273A

# Tab bar colors
active_tab_foreground   #181926
active_tab_background   #C6A0F6
inactive_tab_foreground #CAD3F5
inactive_tab_background #1E2030
tab_bar_background      #181926

# Colors for marks (marked text in the terminal)
mark1_foreground #24273A
mark1_background #B7BDF8
mark2_foreground #24273A
mark2_background #C6A0F6
mark3_foreground #24273A
mark3_background #7DC4E4

# The 16 terminal colors

# black
color0 #494D64
color8 #5B6078

# red
color1 #ED8796
color9 #ED8796

# green
color2  #A6DA95
color10 #A6DA95

# yellow
color3  #EED49F
color11 #EED49F

# blue
color4  #8AADF4
color12 #8AADF4

# magenta
color5  #F5BDE6
color13 #F5BDE6

# cyan
color6  #8BD5CA
color14 #8BD5CA

# white
color7  #B8C0E0
color15 #A5ADCB

'';
}


