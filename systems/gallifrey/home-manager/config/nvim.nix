{pkgs, ...}:{


  programs.neovim = {
  enable = true;
  package = pkgs.neovim-unwrapped;
  plugins = with pkgs; [
  vimPlugins.packer-nvim
  ];
  extraLuaConfig = ''
-- Load Packer
return require('packer').startup(function(use)
    use 'wbthomason/packer.nvim' -- Packer itself

    -- Plugins
    use {
        'VonHeikemen/fine-cmdline.nvim',
        requires = { 'MunifTanjim/nui.nvim' } -- Fine-cmdline with dependency
    }
    use { 'catppuccin/nvim', as = "catppuccin" } -- Catppuccin theme
    use {
        'nvim-lualine/lualine.nvim',
        requires = { 'nvim-tree/nvim-web-devicons', opt = true } -- Lualine statusline with icons
    }
    use {
        "ellisonleao/glow.nvim", -- Markdown preview with Glow
        config = function() require("glow").setup() end
    }
    use 'tpope/vim-fugitive' -- Vim-fugitive for Git integration
    use { 'nvim-telescope/telescope.nvim', -- Telescope fuzzy finder
        requires = { 'nvim-lua/plenary.nvim' }
    }
    use 'nvim-treesitter/nvim-treesitter'
    use 'nvim-orgmode/orgmode'
    use 'nvim-telescope/telescope-file-browser.nvim' -- Telescope file browser
    use 'nvim-lua/plenary.nvim' -- Plenary.nvim dependency
    use 'nvim-tree/nvim-web-devicons' -- Nvim-web-devicons for file icons
    use {
        "goolord/alpha-nvim", -- Alpha-nvim for startup dashboard
        config = function()
            local alpha = require'alpha'
            local dashboard = require'alpha.themes.dashboard'

            -- Set dashboard header
            dashboard.section.header.val = {
                "",
                " ███╗   ██╗ ███████╗ ██████╗  ██╗   ██╗ ██╗ ███╗   ███╗",
                " ████╗  ██║ ██╔════╝██╔═══██╗ ██║   ██║ ██║ ████╗ ████║",
                " ██╔██╗ ██║ █████╗  ██║   ██║ ██║   ██║ ██║ ██╔████╔██║",
                " ██║╚██╗██║ ██╔══╝  ██║   ██║ ╚██╗ ██╔╝ ██║ ██║╚██╔╝██║",
                " ██║ ╚████║ ███████╗╚██████╔╝  ╚████╔╝  ██║ ██║ ╚═╝ ██║",
                " ╚═╝  ╚═══╝ ╚══════╝ ╚═════╝    ╚═══╝   ╚═╝ ╚═╝     ╚═╝",
                "",
                " [ TIP: To exit Neovim, just power off your computer. ] ",
                "",
            }

            -- Set dashboard buttons
            dashboard.section.buttons.val = {
                dashboard.button("ff", "  Find file", ":Telescope find_files<CR>"),
                dashboard.button("fb", "  File browser", ":Telescope file_browser<CR>"),
                dashboard.button("df", "  Dotfiles", ":edit $HOME/Git/dotfiles<CR>"),
                dashboard.button("q", "  Quit NVIM", ":qa<CR>"),
            }

            -- Setup alpha with the configured dashboard
            alpha.setup(dashboard.opts)
        end
    }


  use {
    "kyazdani42/nvim-tree.lua",
    requires = { "kyazdani42/nvim-web-devicons" },
    config = function()
      require("nvim-web-devicons").setup()

      require("nvim-tree").setup {
        hijack_cursor = true,
        view = {
          width = 40
        }
      }
    end
  }


    vim.o.showmode = false


    -- Enable 24-bit color
    vim.opt.termguicolors = true

    -- Set colorscheme and lualine
    vim.cmd('colorscheme catppuccin-macchiato')
    require("lualine").setup {
        options = {
            theme = catppuccin,
            component_separators = "|",
            section_separators = { left = "", right = "" },
        },
        sections = {
            lualine_a = { { "mode", separator = { right = "" }, right_padding = 2 } },
            lualine_b = { "filename", "filesize", "branch", { "diff", colored = false } },
            lualine_c = {},
            lualine_x = {},
            lualine_y = { "filetype" },
            lualine_z = { { "location", separator = { left = "" }, left_padding = 2 } },
        },
        inactive_sections = {
            lualine_a = { "filename" },
            lualine_b = {},
            lualine_c = {},
            lualine_x = {},
            lualine_y = {},
            lualine_z = {},
        },
        tabline = {
            lualine_a = {
                {
                    "buffers",
                    separator = { left = "", right = "" },
                    right_padding = 2,
                    symbols = { alternate_file = "" },
                },
            },
        },
    }

    -- Key mappings
    local builtin = require('telescope.builtin')
    vim.api.nvim_set_keymap('n', ':', '<cmd>FineCmdline<CR>', { noremap = true })
    vim.keymap.set('n', '<Space>ff', builtin.find_files, {}) -- <leader>ff to find files
    vim.keymap.set("n", "<space>fb", ":Telescope file_browser<CR>") -- <space>tf to open Telescope file browser
    vim.keymap.set("n", "<space>bb", ":FineCmdline vnew<CR>") -- <space>bb to open FineCmdline in a new vertical split
    vim.keymap.set("n", "<space>sv", ":FineCmdline vsplit<CR>") -- <space>sv to split vertically
    vim.keymap.set("n", "<space>ss", ":FineCmdline split<CR>") -- <space>ss to split horizontally
    vim.keymap.set("n", "<space>bn", ":bnext<CR>") -- <space>bn to navigate to the next buffer
    vim.api.nvim_set_keymap('n', '<Space>j', '<C-W><C-J>', { noremap = true }) -- Navigate down a split
    vim.api.nvim_set_keymap('n', '<Space>k', '<C-W><C-K>', { noremap = true }) -- Navigate up a split
    vim.keymap.set("n", "<space>df", ":NvimTreeOpen /Users/peter/Git/dotfiles<CR>") 
end)
  '';

};



}
